package org.leadium.http.curl

class CurlResponse(
    val statusCode: Int? = null,
    val body: String? = null
)
