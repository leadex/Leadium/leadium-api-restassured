package org.leadium.http.curl

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.networknt.schema.JsonSchema
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.SpecVersion
import com.networknt.schema.ValidationMessage
import kotlinx.coroutines.runBlocking
import org.apache.log4j.LogManager
import org.junit.jupiter.api.Assertions.assertEquals
import org.leadium.core.OS

object Curl {

    private var log = LogManager.getLogger(this.javaClass)

    private val mapper: ObjectMapper = ObjectMapper()

    fun request(curl: String, statusCode: Int, expectedExitCode: Int = 0): CurlResponse {
        val curlResponse =
            runBlocking {
                executeCurlCommand(curl.trimIndent(), expectedExitCode)
            }
        try {
            assertEquals(statusCode, curlResponse.statusCode!!)
        } catch (e: java.lang.AssertionError) {
            val errorMessageBuilder = java.lang.StringBuilder()
                .append(e.message)
                .append(System.lineSeparator())
                .append(System.lineSeparator())
                .append(curlResponse.body)
            throw AssertionError(errorMessageBuilder.toString())
        }
        return curlResponse
    }

    fun request(curl: String, statusCode: Int, schema: String): CurlResponse {
        val curlResponse = request(curl, statusCode)
        checkSchema(curlResponse.body!!, schema)
        return curlResponse
    }

    fun checkSchema(body: String, schema: String) {
        if (schema.isBlank()) throw AssertionError("Schema is blank!")
        log.info("Response Schema: $schema")
        val schema: JsonSchema = getJsonSchemaFromStringContent(schema)
        val node: JsonNode = mapper.readTree(body)
        val errors: Set<ValidationMessage> = schema.validate(node)
        log.info("Schema Errors: $errors")
        assertEquals(0, errors.size, errors.toString())
    }

    private fun getJsonSchemaFromStringContent(schemaContent: String?): JsonSchema {
        val factory: JsonSchemaFactory = JsonSchemaFactory.getInstance(SpecVersion.VersionFlag.V6)
        return factory.getSchema(schemaContent)
    }

    private suspend fun executeCurlCommand(command: String, expectedExitCode: Int): CurlResponse {
        val command = command.trimIndent() + """ -w " \nstatus code: %{http_code}\n""""
        log.info(command)
        val result = OS.executeCommand(arrayOf("bash", "-c", command))
        log.info("exit code: " + result.exitCode)
        log.info(result.outputStream)
//        log.info(result.errorStream)
        assertEquals(expectedExitCode, result.exitCode)
        try {
            val body = result.outputStream.split("status code: ")[0]
            val statusCode = result.outputStream.split("status code: ")[1].toInt()
            return CurlResponse(statusCode, body)
        } catch (e: java.lang.IndexOutOfBoundsException) {
            throw IndexOutOfBoundsException(result.outputStream)
        }
    }
}