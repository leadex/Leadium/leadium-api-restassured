package org.leadium.http.retrofit

import com.google.gson.FieldNamingPolicy.UPPER_CAMEL_CASE
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import javax.net.ssl.HostnameVerifier

object RetrofitClient {

    private val gson = GsonBuilder()
//        .enableComplexMapKeySerialization()
//        .serializeNulls()
        .setFieldNamingStrategy(UPPER_CAMEL_CASE)
//        .setVersion(1.0)
        .setPrettyPrinting()
        .create()

    fun <T> createService(service: Class<T>, baseUrl: String): T =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(HttpClient.generateUnsecureOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(service)

    fun <T> createService(service: Class<T>, baseUrl: String, hostnameVerifier: HostnameVerifier): T =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(HttpClient.generateUnsecureOkHttpClient(hostnameVerifier))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(service)

    fun <T> createService(service: Class<T>, baseUrl: String, certificate: File, password: String): T =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(HttpClient.generateSecureOkHttpClient(certificate, password))
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(service)

    private fun getClient(certificate: File?, password: String) =
        if (certificate == null) HttpClient.generateUnsecureOkHttpClient()
        else HttpClient.generateSecureOkHttpClient(certificate, password)
}