package org.leadium.http.retrofit

import io.qameta.allure.okhttp3.AllureOkHttp3
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import org.apache.log4j.Logger.getLogger
import java.io.File
import java.io.FileInputStream
import java.security.KeyStore
import java.security.SecureRandom
import java.time.Duration
import java.util.concurrent.TimeUnit.SECONDS
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

object HttpClient {

    private val timeout = Duration.ofSeconds(10)
    private val log = getLogger(this::class.java)
    private val loggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { msg -> log.debug(msg) })
        .setLevel(BODY)

    /**
     * Generates an OkHttpClient with our trusted CAs
     * to make calls to a service which requires it.
     *
     * @param context the context to access our file.
     * @return OkHttpClient with our trusted CAs added.
     */
    fun generateUnsecureOkHttpClient(): OkHttpClient {
        // Create a simple builder for our http client, this is only por example purposes
        val httpClientBuilder = OkHttpClient.Builder()
            .readTimeout(60, SECONDS)
            .connectTimeout(60, SECONDS)
        // Here you may wanna add some headers or custom setting for your builder
        //Finally set the sslSocketFactory to our builder and build it
        return httpClientBuilder
            .retryOnConnectionFailure(true)
            .callTimeout(timeout)
            .connectTimeout(timeout)
            .readTimeout(timeout)
            .writeTimeout(timeout)
            .addInterceptor(AllureOkHttp3())
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain: Interceptor.Chain ->
                val builder = chain.request().newBuilder()
                chain.proceed(builder.build())
            }
            .build()
    }

    fun generateUnsecureOkHttpClient(hostnameVerifier: HostnameVerifier): OkHttpClient {
        // Create a simple builder for our http client, this is only por example purposes
        val httpClientBuilder = OkHttpClient.Builder()
            .readTimeout(60, SECONDS)
            .connectTimeout(60, SECONDS)
        // Here you may wanna add some headers or custom setting for your builder
        //Finally set the sslSocketFactory to our builder and build it
        return httpClientBuilder
            .retryOnConnectionFailure(true)
            .callTimeout(timeout)
            .connectTimeout(timeout)
            .readTimeout(timeout)
            .writeTimeout(timeout)
            .addInterceptor(AllureOkHttp3())
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain: Interceptor.Chain ->
                val builder = chain.request().newBuilder()
                chain.proceed(builder.build())
            }
            .hostnameVerifier(hostnameVerifier)
            .build()
    }

    fun generateSecureOkHttpClient(certificate: File, password: String): OkHttpClient {
        // Create a simple builder for our http client, this is only por example purposes
        val httpClientBuilder = OkHttpClient.Builder()
            .readTimeout(60, SECONDS)
            .connectTimeout(60, SECONDS)
        // Here you may wanna add some headers or custom setting for your builder
        //Finally set the sslSocketFactory to our builder and build it
        return httpClientBuilder
            .sslSocketFactory(getSslContext(certificate, password).socketFactory, TrustManager())
            .retryOnConnectionFailure(true)
            .callTimeout(timeout)
            .connectTimeout(timeout)
            .readTimeout(timeout)
            .writeTimeout(timeout)
            .addInterceptor(AllureOkHttp3())
            .addInterceptor(loggingInterceptor)
            .addInterceptor { chain: Interceptor.Chain ->
                val builder = chain.request().newBuilder()
                chain.proceed(builder.build())
            }
            .build()
    }

    private fun getSslContext(certificate: File, password: String = "changeit"): SSLContext {
        // Get the file of our certificate
        val caFileInputStream = FileInputStream(certificate)

        // We're going to put our certificates in a Keystore
        val keyStore = KeyStore.getInstance("PKCS12")
        keyStore.load(caFileInputStream, password.toCharArray())

        // Create a KeyManagerFactory with our specific algorithm our public keys
        // Most of the cases is gonna be "X509"
        val keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
        keyManagerFactory.init(keyStore, password.toCharArray())

        // Create a SSL context with the key managers of the KeyManagerFactory
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(keyManagerFactory.keyManagers, null, SecureRandom())
        return sslContext
    }

    class TrustManager : X509TrustManager {

        override fun checkServerTrusted(
            p0: Array<out java.security.cert.X509Certificate>?,
            p1: String?
        ) {
            //allow all
        }

        override fun checkClientTrusted(
            p0: Array<out java.security.cert.X509Certificate>?,
            p1: String?
        ) {
            //allow all
        }

        override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
            return arrayOf()
        }
    }
}