package org.leadium.http.restassured

import io.restassured.RestAssured
import io.restassured.response.Response
import org.apache.log4j.Logger
import org.junit.jupiter.api.Assertions
import java.net.SocketException

interface RestAssuredSyntax {

    /**
     * Method uses preemptive http basic authentication.
     * This means that the authentication details are sent in the request
     * header regardless if the server has challenged for authentication or not.
     * @param baseUrl
     * @param login The user name
     * @param pass The password
     */
    fun initRestAssured(baseUrl: String, login: String, pass: String) {
        RestAssured.baseURI = baseUrl
        RestAssured.authentication = RestAssured.preemptive().basic(login, pass)
    }

    /**
     * Method performs a GET request to a specified path,
     * attaches Request/Response to Allure report,
     * @param url String
     * @param expectedStatusCode Int
     * @return response
     */
    fun httpRequest(url: String, requestMethod: () -> Response, expectedStatusCode: Int): Response {
        val url = "${RestAssured.baseURI}/$url"
        Logger.getLogger("Rest Assured").info("Request: $url")
        val response = tryToSendRequest { requestMethod.invoke() }
//        attachJson("Response: $url", response.prettyPrint())
        validateStatusCode(response, expectedStatusCode)
        return response
    }

    /**
     * @suppress
     */
    private fun tryToSendRequest(requestSpecificationMethod: () -> Response): Response {
        val response: Response? = null
        return try {
            requestSpecificationMethod.invoke()
        } catch (e: SocketException) {
            Assertions.fail<String>(e.localizedMessage)
            response!!
        }
    }

    /**
     * @suppress
     */
    private fun validateStatusCode(response: Response, expectedStatusCode: Int) {
        with(response.then()) {
            statusCode(expectedStatusCode)
        }
    }
}